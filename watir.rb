require 'watir'

# Settings | 1: AnimeZone/KreskówkaZone, 2: Animek24, 3: CDA, 9: fili.tv, 10: Upload to Best-Anime
webs = ARGV[0].to_i
if(webs != 10)
	src = ARGV[4] else src = 'http://localhost:3000/login' end
movie = ARGV[1]
season = ARGV[2].to_i
episode = ARGV[3].to_i
# IF Upload to Best-Anime
episodes = ARGV[2].to_i
position = ARGV[3].to_i

if webs!=10
	file = File.new movie.to_s, "w"
else
	file = File.new movie.to_s, "r"
end


# Functions
def rm_ads(browser)
	title = browser.title
	if(title.match('anime'))
		browser.windows.last.close
	else
		browser.window.close
	end
end

def add_record(browser, movie, season, episode, webs, file)
	if webs==1 # AnimeZone/KreskówkaZone
		#sleep 2

		if browser.title.match("anime")
			#name=browser.element(xpath: '//div[@class="panel-body"]/h3').text[11..-1] if browser.element(xpath: '//div[@class="panel-body"]/h3').text[-1]!='.'
			#name=browser.element(xpath: '//div[@class="panel-body"]/h3').text[11..-2] if browser.element(xpath: '//div[@class="panel-body"]/h3').text[-1]=='.'
			name=browser.element(xpath: '//div[@class="panel-body"]/h3').text[11..-1] if browser.element(xpath: '//div[@class="panel-body"]/h3').text[11]!=' '
			name=browser.element(xpath: '//div[@class="panel-body"]/h3').text[12..-1] if browser.element(xpath: '//div[@class="panel-body"]/h3').text[11]==' '

			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+name.to_s+';'+'src="'+browser.iframe.src+'";'+'Napisy;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+name.to_s+';'+'src="'+browser.iframe.src+'";'+'Napisy;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+name.to_s+';'+'src="'+browser.iframe.src+'";'+'Napisy;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+name.to_s+';'+'src="'+browser.iframe.src+'";'+'Napisy;')
			end
		elsif browser.title.match("kreskówki")
			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+browser.element(xpath: '//table/tbody/tr[2]/td').text+';'+'src="'+browser.iframe.src+'";'+'Lek/Dub;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+browser.element(xpath: '//table/tbody/tr[2]/td').text+';'+'src="'+browser.iframe.src+'";'+'Lek/Dub;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+browser.element(xpath: '//table/tbody/tr[2]/td').text+';'+'src="'+browser.iframe.src+'";'+'Lek/Dub;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+browser.element(xpath: '//table/tbody/tr[2]/td').text+';'+'src="'+browser.iframe.src+'";'+'Lek/Dub;')
			end
		end
	elsif webs==2 # Animek24
		sleep 2
		if browser.div(id: "option-1").iframe.exist?
			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+browser.element(xpath: '//h1[@class="epih1"]').text+';'+'src="'+browser.div(id: "option-1").iframe.src+'";'+'Lek/Dub;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+browser.element(xpath: '//h1[@class="epih1"]').text+';'+'src="'+browser.div(id: "option-1").iframe.src+'";'+'Lek/Dub;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+browser.element(xpath: '//h1[@class="epih1"]').text+';'+'src="'+browser.div(id: "option-1").iframe.src+'";'+'Lek/Dub;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+browser.element(xpath: '//h1[@class="epih1"]').text+';'+'src="'+browser.div(id: "option-1").iframe.src+'";'+'Lek/Dub;')
			end
		end
	elsif webs==3 # CDA
		sleep 2

    sources = browser.elements(xpath: '//div[@class="thumb-wrapper-just"]/div[@class="list-when-small tip"]/span[@class="thumbnail viewList-inline"]/span[@class="caption"]/span[@class="caption-label"]')
    
		sources.each do |source|
			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+';'+'src="https://ebd.cda.pl/1160x654/'+source.a.href[25..-1]+'";'+'Lek/Dub;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+';'+'src="https://ebd.cda.pl/1160x654/'+source.a.href[25..-1]+'";'+'Lek/Dub;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+';'+'src="https://ebd.cda.pl/1160x654/'+source.a.href[25..-1]+'";'+'Lek/Dub;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+';'+'src="https://ebd.cda.pl/1160x654/'+source.a.href[25..-1]+'";'+'Lek/Dub;')
			end

			episode+=1
		end
	elsif webs==9 # fili.tv
		sleep 2

		if !browser.iframe.iframe.text.match("Sorry") && !browser.iframe.iframe.text.match("File was deleted") && !browser.iframe.iframe.text.match("not found") && !browser.iframe.iframe.src.match("openload")
			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+browser.h4.text[9..-1]+';'+'src="'+browser.iframe().iframe().src+'";'+'Lek/Dub;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+browser.h4.text[9..-1]+';'+'src="'+browser.iframe().iframe().src+'";'+'Lek/Dub;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+browser.h4.text[9..-1]+';'+'src="'+browser.iframe().iframe().src+'";'+'Lek/Dub;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+browser.h4.text[9..-1]+';'+'src="'+browser.iframe().iframe().src+'";'+'Lek/Dub;')
			end
		end
	end
end

browser = Watir::Browser.new :chrome, :switches => %w[--load-extension=/home/USERNAME/.config/google-chrome/Default/Extensions/cjpalhdlnbpafiamejdnhcphjbkeiagm/1.15.2_0]

browser.goto src
browser.window.maximize

case webs
	when 1	# AnimeZone/KreskówkaZone
		while true

			sources=1
			tabPlayers =
			[
				["Myvi.ru", 			"Myvi.tv",				"Ошибка сервера"],
			  ["Mp4Upload.com", "Mp4upload.com",	"File was deleted"], # The media could not be loaded
			  ["ok.ru", 				"Ok.ru",					"been"],	# Video has not been found
			  ["Vidlox.tv", 		"Vidlox.tv",			"Error"],
			  ["Sibnet", 				"Sibnet",					"Ошибка обработки видео"], # Нет такого видео
			  ["Facebook", 			"Facebook",				"Film niedostępny"],
			  ["Google.com", 		"Google.com",			"Żądany plik nie istnieje"],
				["CDA.pl", 				"CDA.pl",					"link został usunięty"],
			  ["VidFile.net", 	"VidFile.net",		"Video not found"],
				["thevideo.me",		"thevideo.me",		"File was deleted"],
				["Meta.ua", 			"Meta.ua",				"Ошибка"],
				["Streamin.to",		"Streamin.to",		"File was deleted"],
				["Rutube",				"Rutube",					"Вы пробуете посмотреть"],
				["Vidto.me",			"Vidto.me",				"File Does not Exist"],
				["AnyFiles", 			"Anyfiles",				"Wideo niedostępne"],
				["Tune.pk",				"Tune.pk",				"video has been deactivated"],
				["Freedisc.pl",		"Freedisc.pl",		"Ten plik"],
				["Vidzer.net",		"Vidzer.net",			"Not Found"],
				#["vShare.io", 		"vShare.io",			"Can't find the resource"],
				["videowood.tv"		"videowood.tv",		"This video doesn't exist"],
				["Cloudtime.to",	"Cloudtime.to",		"This video is not yet ready"],
				#["MovShare.net",	"MovShare.net",		"This video is not yet ready"],
				["openload.io",		"openload.io",		"We’re Sorry!"],
				#["flashx.tv",			"flashx.tv",			"x-x"]
			]

			if browser.title.match("anime")

				tabPlayers.length.times do |index|
					break if sources==0
					browser.elements(xpath: '//tbody/tr/td[3]/span[@class="sprites PL lang"]').size.times do |i|
						if browser.element(xpath: '//tbody/tr['+(i+1).to_s+']/td[1]').text.match(tabPlayers[index][0])
							browser.button(xpath: "//tbody/tr["+(i+1).to_s+"]/td[4]/button[@class='btn btn-xs btn-success play']").click
							sleep 1

							if !browser.iframe.exist? #Ad after click
								rm_ads browser
								browser.button(xpath: "//tbody/tr["+(i+1).to_s+"]/td[4]/button[@class='btn btn-xs btn-success play']").click
							end

							next if browser.iframe.text.match(tabPlayers[index][2])
							sources-=1
							add_record browser, movie, season, episode, webs, file
							break
						end
					end
				end

				episode+=1
				if browser.li(class: "next").a.exists?
					browser.li(class: "next").a.click
				else
					browser.close
					break
				end

			elsif browser.title.match("kreskówki")
				#browser.execute_script('return document.getElementsByClassName("pogrubione")[1].style.display = "block";')
				#browser.driver.execute_script("window.scrollBy(0,200)")

				tabPlayers.length.times do |index|
					break if sources==0
					browser.elements(xpath: '//tbody/tr/td[4]/span[@class="sprites pl center"]').size.times do |i|
						if browser.element(xpath: '//tbody/tr[@class="wiersz"]['+(i+1).to_s+']/td[2]').text.match(tabPlayers[index][1]) && !browser.element(xpath: '//tbody/tr[@class="wiersz"]['+(i+1).to_s+']/td[3]').text.match("Napisy")
							browser.driver.execute_script("window.scrollBy(0,200)")
							#browser.execute_script('return document.getElementsByClassName("play")['+i.to_s+'].style.width = "100%";'); sleep 2
							browser.element(xpath: '//tbody/tr[@class="wiersz"]['+(i+1).to_s+']/td[5]/a').click
							sleep 1
							next if browser.iframe.text.match(tabPlayers[index][2])
							sources-=1
							add_record browser, movie, season, episode, webs, file
							#browser.driver.execute_script("window.scrollBy(0,300)")
							break
						end
					end
				end

				episode+=1
				if browser.element(xpath: '//table[4]/tbody/tr/td[3]/a').exists?
					browser.element(xpath: '//table[4]/tbody/tr/td[3]/a').click
				else
					browser.close
					break
				end

			end
		end
	when 2	# Animek24
		sleep 1
		while true
			#sleep 11
			add_record browser, movie, season, episode, webs, file
			episode+=1
			break if browser.element(xpath: '//div[@class="pag_episodes"]/div[@class="item"][3]/a[@class="nonex"]').exists?
			browser.element(xpath: '//div[@class="pag_episodes"]/div[@class="item"][3]/a').click
		end
	when 3	# CDA
		sleep 1
    if browser.element(xpath: '//h3[@class="text-center register-name"]').exists?
      browser.element(xpath: '//li[@class="dropdown sep"]/a').click
      browser.text_field(name: "username").set("login@poczta.domena")
      browser.text_field(name: "password").set("hasło")
      browser.element(xpath: '//div[@class="col-md-5"]/input').click
    end
    browser.element(xpath: '//div[@class="btn-group pull-right"]/div[2]').click
    browser.element(xpath: '//li[@class="ListaMalychFolderow"]/a').click
    
		add_record browser, movie, season, episode, webs, file
	when 9	# Fili.tv
		sleep 1
		#browser.element(xpath: "//div[@title='Przechodzi do następnego sezonu']").click
		#puts browser.div(class: "overlay").ul.a.li(class: "l_active").text
		#puts browser.element(xpath: "//div[@class='overlay']/ul/a[10]").text

		while true
			browser.execute_script('return document.getElementsByClassName("overlay")[0].style.maxHeight = "none";')
			while browser.element(xpath: "//div[@class='overlay']/ul/a["+episode.to_s+"]").exists?
				browser.element(xpath: "//div[@class='overlay']/ul/a["+episode.to_s+"]").click

				if browser.element(xpath: "//ul[1]/li[@class='line']/div[2]/div/span").exists?
					(browser.elements(xpath: "//ul[1]/li[@class='line']/div[2]/div/span").size).times do |num_src|
            next if num_src==0
						if num_src!=0 && browser.element(xpath: "//li[@class='line']["+num_src.to_s+"]/div[1]/span[1]").text == "openload"
							next
						end
						if num_src!=0
							browser.element(xpath: "//li[@class='line']["+num_src.to_s+"]/div[2]/div/span").click
						end
            sleep 1
						while browser.iframe.iframe.src.match("recaptcha")
							sleep 5
						end
						add_record browser, movie, season, episode, webs, file
					end
				end

				episode+=1
			end
			season+=1
			if browser.element(xpath: "//div[@title='Przechodzi do następnego sezonu']").exists?
				episode = 1
				browser.element(xpath: "//div[@title='Przechodzi do następnego sezonu']").click
			else
				browser.close
				break
			end
		end
	when 10	# Upload to Best-Anime
		browser.text_field(type: "email").set('login@poczta.domena')
		browser.text_field(type: "password").set('hasło')
		browser.input(type: "submit").click

		browser.element(xpath: "//nav/div/ul[2]/a").click
		browser.element(xpath: "//nav/div/ul[2]/div/a").click
		sleep 1
		browser.element(xpath: "//nav/div/ul[2]/li/a").click

		#	Start Upload
		movie, title, src, version = [], [], [], []

		episodes.times do |loop|
			num_src = 1

			file.each do |row|
				if loop>0 && num_src == 1
					movie_copy=movie.last; title_copy=title.last; src_copy=src.last; version_copy=version.last
					movie.clear; title.clear; src.clear; version.clear
					movie[0]=movie_copy; title[0]=title_copy; src[0]=src_copy; version[0]=version_copy
					num_src+=1
				end
																						movie[num_src-1] = row[0,row.index(";")]
					row = row[row.index(";")+1..-1];	title[num_src-1] = row[0,row.index(";")]
					row = row[row.index(";")+1..-1];	src[num_src-1] = row[0,row.index(";")]
					row = row[row.index(";")+1..-1];	version[num_src-1] = row[0,row.index(";")]

				if num_src==1
					num_src+=1
					next
				end

				if title[num_src-1]!=title[num_src-2]
					break;
				end
				num_src+=1
			end

			# Control end file for one source
			if num_src==1
				num_src+=1; movie[0], title[0], src[0], version[0] = movie[1], title[1], src[1], version[1]
			end

			sleep 1
			browser.select_list(id: "episode_movie_id").select(ARGV[1])
			browser.text_field(id: "episode_title").set(title[0].to_s)
			browser.select_list(:id, "episode_position").select(position.to_s)
			if (num_src-1)>=6
				browser.select_list(name: "selectSources").select("Sześć")
			elsif (num_src-1)==5
				browser.select_list(name: "selectSources").select("Pięć")
			elsif (num_src-1)==4
				browser.select_list(name: "selectSources").select("Cztery")
			elsif (num_src-1)==3
				browser.select_list(name: "selectSources").select("Trzy")
			elsif (num_src-1)==2
				browser.select_list(name: "selectSources").select("Dwa")
			elsif (num_src-1)==1
				browser.select_list(name: "selectSources").select("Jedno")
			end

			sleep 1
			browser.driver.execute_script("window.scrollBy(0,400)")

			(num_src-1).times do |i|
				browser.text_field(xpath: "//tr[@class='generated']["+(i+1).to_s+"]/td[2]/div/input[@class='form-control iframe-gen']").set(src[i].to_s)
				browser.element(xpath: '//tr[@class="generated"]['+(i+1).to_s+']/td[2]/div').select_list.select("Napisy") if version[i] == "Napisy"

				sleep 1
				if i==5
					break
				end
			end

			browser.button(xpath: '//input[@name="commit"]').click
			sleep 1
			browser.element(xpath: "//nav/div/ul[2]/li/a").click

			position+=1
		end

	else
		puts "webs argument not walid"
end


browser.close
