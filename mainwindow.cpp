#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <cstdio>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_downloadSrc_clicked()
{
    /*ui->labelSrc->setText(tr("Kliknięto! :)"));
    QString ls = static_cast<QString>(ui->webs->currentIndex());
    ui->labelTitle->setText(ls);*/

    std::string webs = ui->webs->currentText().toStdString();
    std::string webs2 = ui->webs2->currentText().toStdString();
    std::string title = ui->title->text().toStdString();
    std::string season = ui->season->text().toStdString();
    std::string episode = ui->episode->text().toStdString();
    std::string src = ui->src->text().toStdString();
    std::string src2 = ui->src2->text().toStdString();

    if(webs == "AnimeZone/KreskowkaZone") webs="1";
    else if(webs == "Animek24") webs="2";
    else if(webs == "CDA") webs="3";
    else if(webs == "Google Drive") webs="4";
    else if(webs == "FreeDisc") webs="5";
    else if(webs == "Fili.tv") webs="9";

    if(webs2 == "AnimeZone/KreskowkaZone") webs2="1";
    else if(webs2 == "Animek24") webs2="2";
    else if(webs2 == "CDA") webs2="3";
    else if(webs2 == "Google Drive") webs2="4";
    else if(webs2 == "FreeDisc") webs2="5";
    else if(webs2 == "Fili.tv") webs2="9";

    std::string cmd = "ruby capybara.rb "+webs+" \""+title+"\" "+season+" "+episode+" "+src;
    std::string cmd2 = "ruby capybara.rb "+webs2+" \""+title+" -napisy\" "+season+" "+episode+" "+src2;

    system(cmd.c_str());    //std::cout<<cmd<<std::endl;
    system(cmd2.c_str());   //std::cout<<cmd2<<std::endl;

    //Dostosowywanie pliku z napisami
    //std::fstream file(title+" -napisy", std::ios::in);
}

void MainWindow::on_joinSrc_clicked()
{
    struct dirent **namelist;

    int n;
    std::string swap;

    n = scandir(".", &namelist, 0, alphasort);

    if(n<0)
        perror("scandir");
    else{
        while(--n>2){
            if((std::string)(namelist[n]->d_name) == (std::string)(namelist[n-1]->d_name)+" -napisy"){
                //std::cout<<namelist[n]->d_name<<std::endl;

                int count[3] {0,0,0};
                std::string *data[3];
                /*for(int i=0; i<2; i++)
                        data[i] = new std::string[2];*/

                std::fstream file(namelist[n-1]->d_name, std::ios::in);
                while(!file.eof()){ getline(file, swap); count[0]++; }
                //std::cout<<namelist[n-1]->d_name<<std::endl;
                file.close(); file.open(namelist[n-1]->d_name, std::ios::in);

                data[0] = new std::string[count[0]];

                for(int i=0; i<count[0]; i++){
                    std::getline(file, data[0][i]);
                    //std::cout<<data[0][i]<<std::endl;
                } file.close();

                file.open(namelist[n]->d_name, std::ios::in);
                while(!file.eof()){ getline(file, swap); count[1]++; }
                //std::cout<<namelist[n]->d_name<<std::endl;
                file.close(); file.open(namelist[n]->d_name, std::ios::in);
                data[1] = new std::string[count[1]];

                for(int i=0; i<count[1]; i++){
                    std::getline(file, data[1][i]);
                    //std::cout<<data[1][i]<<std::endl;
                } file.close();

                //swap = data[0][0].substr((std::string)(namelist[n-1]->d_name), data[0][0].find(" - "));
                /*std::string title = namelist[n-1]->d_name;
                    swap = data[0][0].substr(title.length()+1, 4);
                    std::cout<<swap<<std::endl;*/

                std::string title = namelist[n-1]->d_name;

                for(int i=0; i<count[1]-1; i++){
                    data[1][i] = data[1][i].substr(0, title.length())+";"+data[1][i].substr(title.length()+9, -1)+"\n";
                }

                file.open(title+" -napisy", std::ios::out);
                for(int i=0; i<count[0]-1; i++){
                    //swap = data[0][i].substr(title.length()+1, 4);
                    if(i==0)
                        file<<data[0][i]<<std::endl;
                    else if(data[0][i].substr(title.length()+1, 4) == data[0][i-1].substr(title.length()+1, 4))
                        file<<data[0][i]<<std::endl;
                    else{
                        for(int j=0; j<count[1]-1; j++){
                            if(data[0][i-1].substr(title.length()+1, 4) == data[1][j].substr(title.length()+1, 4))
                                file<<data[1][j];
                        }
                        file<<data[0][i]<<std::endl;
                    }
                    if(i+1==count[0]-1)
                        for(int j=0; j<count[1]-1; j++){
                            if(data[0][i].substr(title.length()+1, 4) == data[1][j].substr(title.length()+1, 4))
                                file<<data[1][j];
                        }

                } file.close();

                //Błąd jest poniżej:
                //Rewriting data with corrected
                file.open(title+" -napisy", std::ios::in);
                while(!file.eof()){ getline(file, swap); count[2]++; }
                file.close();
                data[2] = new std::string[count[2]];

                file.open(title+" -napisy", std::ios::in);
                for(int i=0; i<count[2]; i++){
                    std::getline(file, data[2][i]);
                } file.close();

                file.open(title, std::ios::out);
                for(int i=0; i<count[2]-2; i++){
                    file<<data[2][i].substr(0,title.length()+1);
                    if(data[2][i].substr(title.length()+1, 4) == data[2][i+1].substr(title.length()+1, 4) && data[2][i].substr(title.length()+1, data[2][i].find(";src")).length() < data[2][i+1].substr(title.length()+1, data[2][i+1].find(";src")).length()){
                        if(data[2][i+1].find(".;")==std::string::npos) file<<data[2][i+1].substr(title.length()+1, data[2][i+1].substr(title.length()+1, data[2][i+1].find(";src")).length()-title.length());
                        else file<<data[2][i+1].substr(title.length()+1, data[2][i+1].substr(title.length()+1, data[2][i+1].find(";src")).length()-title.length()-2)<<';';
                    } else
                        if(data[2][i].find(".;")==std::string::npos) file<<data[2][i].substr(title.length()+1, data[2][i].substr(title.length()+1, data[2][i].find(";src")).length()-title.length());
                        else file<<data[2][i].substr(title.length()+1, data[2][i].substr(title.length()+1, data[2][i].find(";src")).length()-title.length()-2)<<';';
                    file<<data[2][i].substr(data[2][i].find(";src")+1, -1)<<std::endl;

                    if(i==count[2]-3) { if(data[2][i+1].find(".;")!=std::string::npos) data[2][i+1].replace(data[2][i+1].find(".;"),2,";"); file<<data[2][i+1]; }
                    //if(i==count[2]-3) { if(data[2][i+1].find(".;")<=0) data[2][i+1].replace(data[2][i+1].find(".;"),2,";"); file<<data[2][i+1]; }
                } file.close();
                system(("rm \""+title+" -napisy\"").c_str());
                //std::remove((const char*)(title)+" -napisy");

            }
            //std::cout<<namelist[n]->d_name<<std::endl;
            free(namelist[n]);
        }
    }

    free(namelist[1]); free(namelist[0]); free(namelist);
}

void MainWindow::on_uploadToBA_clicked()
{
    std::string title = ui->title->text().toStdString();
    std::string episodes = ui->episodes->text().toStdString();
    std::string position = ui->position->text().toStdString();
    std::string cmd = "ruby capybara.rb 10 \""+title+"\" "+episodes+" "+position;
    system(cmd.c_str());
}

void MainWindow::on_title_textChanged(const QString &arg1)
{
    ui->labelTitleActive->setText(arg1);
}
