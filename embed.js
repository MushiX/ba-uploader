const path = require('path');
const fs = require('fs');
const readline = require('readline');
const Auth = require('./auth.js');
const {Builder, Capabilities, By, Key, until} = require('selenium-webdriver');

const encodeExt = file => {
	const stream = fs.readFileSync(path.resolve(file));
	return Buffer.from(stream).toString('base64');
};

if(process.argv.length < 3)
{
	console.log('BAUploader Script Usage:');
	console.log('Type operation:');
	console.log('node embed.js C [Type Movie] [Start Letter] [Start Movie]\t\t<- Create list not working streams on best-anime.eu')
	console.log('node embed.js D [Series name] [Season] [Episode] [URL]\t\t\t<- Create CSV with iframes this serie');
	console.log('node embed.js U [Filename] [Start position] [How many episodes]\t\t<- Send iframes to best-anime.eu service');
	process.exit();
}

var action = process.argv[2];
var url = 'https://best-anime.eu/login';
switch(action)
{
	case 'C':	var movieType = process.argv[3];
				var letterStart = process.argv[4];
				break;
	case 'D':	var movie = process.argv[3];
				var season = process.argv[4];
				var episode = process.argv[5];
				url = process.argv[6];
				break;
	case 'U':	var movie = process.argv[3];
				var startPosition = process.argv[4];
				break;
	default:	console.log('This action is not exist use: C [Check], D [Download], U [Update]');
				process.exit();
}

let webdriver = require('selenium-webdriver');
let chrome = require('selenium-webdriver/chrome');

let driver = new webdriver.Builder()
	.withCapabilities(Capabilities.chrome().setPageLoadStrategy('normal'))
	.setChromeOptions(new chrome.Options()
// 		.addExtensions(encodeExt('./uBlock.crx'))
		.addArguments('start-maximized')
		.setChromeBinaryPath('/usr/bin/brave')
	)
	.build();

(async function start(){
	try {
		await driver.get(url);

		if(action == 'C')
		{

		}

		if(action == 'D')
		{
			let path = '';

			switch(true)
			{
				case url.includes('animek24.blogspot.com'):
					// await driver.switchTo().frame(await driver.findElement(By.css('#scmframe')));
					// await driver.switchTo().frame(await driver.findElement(By.css('#content')));
					let posts = await driver.findElements(By.css('.post'));

					for(let post of posts)
					{
						if(await post.findElement(By.css('.post-title a')).getText() == movie)
						{
							console.log(await post.findElement(By.css('.post-title a')).getText());
							let divs = await post.findElements(By.css('.post-body div'));
							for(let div of divs)
							{
								await div.findElements(By.css('div')).then((elems) => {
									for(let elem of elems)
									{
										elem.findElements(By.css('div')).then((elems) => {
											for(let elem of elems)
											{
												//do links także trzeba dodać then -> err
												let links = elem.findElements(By.css('a[target="_blank"]'));
												for(let link of links)
												{
													console.log(link);
												}
											}
										}, (err)=>{})
										let links = elem.findElements(By.css('a[target="_blank"]'));
										for(let link of links)
										{
											console.log(link);
										}
									}
								}, (err)=>{})
								let links = elem.findElements(By.css('a[target="_blank"]'));
								for(let link of links)
								{
									console.log(link);
								}
							}
						}
					}
					break;

				case url.includes('animek24.pl'):
					break;

				case url.includes('animeni.pl'):
					do {
						await new Promise(resolve => setTimeout(resolve, 1000));
						let source = await driver.wait(until.elementLocated(By.xpath('//iframe')),15000).getAttribute('src');
						let episode = await driver.findElement(By.xpath('//div[@class="headlist"]/div[@class="det"]/span')).getText();
						episode = (episode.split(' - ')[1]).split(' / ')[0];

						let title = season;
						if(season<10) title += 'x';
						title += episode + ' - ';

						fs.appendFile(movie, movie+';'+title+';'+source+';'+'Lek/Dub;\n', ()=>{}, ()=>{});
					} while(await driver.findElement(By.xpath('//a[@rel="next"]')).then((elem) => {
						elem.click();
						return true;
					}, (err)=>{return false;}))
					break;

				case url.includes('cda.pl'):
					await driver.findElement(By.css('h3.register-name, .panel-body .alert-warning')).then((elem) => {
						driver.findElement(By.css('li.dropdown a.dropdown-toggle.bold-menu')).click();
						driver.findElement(By.name('username')).sendKeys(Auth.CDA_LOGIN);
						driver.findElement(By.name('password')).sendKeys(Auth.CDA_PASSWORD);
						driver.findElement(By.css('.col-md-5 input')).click();
					}, (err)=>{})

					await new Promise(resolve => setTimeout(resolve, 2000));

					await driver.findElement(By.css('.btn-group.pull-right div:nth-child(2) button')).click();
					await driver.findElement(By.css('.ListaMalychFolderow a')).click();
					path = '.thumb-wrapper-just .list-when-small.tip .thumbnail.viewList-inline .caption .caption-label a';

					do{
						await driver.wait(until.elementLocated(By.css(path)),10000);
						let sources = await driver.findElements(By.css(path));

						for(let source of sources)
						{
							let src = await source.getAttribute('href');
							src = 'https://ebd.cda.pl/1160x654/'+src.substr(25);

							let title = season;
							if(season<10)
								title += 'x';
							if(episode<10)
								title += '0';
							title += episode+' - ';

							fs.appendFile(movie, movie+';'+title+';'+src+';'+'Lek/Dub;\n', ()=>{}, ()=>{});
							++episode;
						}
					} while(await driver.findElement(By.css('.paginationControl a.next')).then((elem) => {
						elem.click();
						return true;
					}, (err)=>{return false;}));

					break;

				case url.includes('google.com'):
					path = '.iZmuQc c-wiz div[data-id]';

					let sources = await driver.findElements(By.css(path));
					for(let source of sources)
					{
						let src = await source.getAttribute('data-id');
						src = 'https://drive.google.com/file/d/'+src+'/preview';

						let title = season;
						if(season<10)
							title += 'x';
						if(episode<10)
							title += '0';
						title += episode+' - ';

						fs.appendFile(movie, movie+';'+title+';'+src+';'+'Lek/Dub;\n', ()=>{}, ()=>{});
						++episode;
					}
					break;

				case url.includes('mp4upload.com'):
					break;

				case url.includes('freedisk.com'):
					break;

				case url.includes('www.animezone.pl'):
					break;

				default:
					console.log('Źródło nie jest obsługiwane');
			}
		}

		if(action == 'U')
		{
			await driver.findElement(By.css('#session_email')).sendKeys(Auth.BA_LOGIN);
			await driver.findElement(By.css('#session_password')).sendKeys(Auth.BA_PASSWORD);
			await driver.findElement(By.css('input[type="submit"]')).click();

			while(await driver.findElement(By.css('iframe[title="Zadanie reCAPTCHA"]')).then(() => {
					return true;
				}, (err)=>{return false;})) {
				await new Promise(resolve => setTimeout(resolve, 2000));
			}

			await driver.wait(until.elementLocated(By.css('#navbarCollapse ul:last-child a')),10000);
			await driver.findElement(By.css('#navbarCollapse ul:last-child a')).click();
			await driver.findElement(By.css('#navbarCollapse ul:last-child div a')).click();

			await driver.wait(until.elementLocated(By.css('.navbar-light input[name="search"]')),10000);
			await driver.findElement(By.name('search')).sendKeys(movie);
			await driver.findElement(By.css('.navbar-light .form-inline input[type="submit"]')).click();

			await driver.wait(until.elementLocated(By.css('tbody tr:first-child .btn-success')),10000);
			await driver.findElement(By.css('tbody tr:first-child .btn-success')).click();

			const lineReader = readline.createInterface({
				input: fs.createReadStream(movie),
				crlfDelay: Infinity
			});

			for await (const line of lineReader) {
				data = line.split(';');
				episode = data[1].split(' - ')[0];
				sources = data[2].split(',');

				await driver.wait(until.elementLocated(By.xpath('//td[@class="title"][contains(text(), "'+episode+'")]')),2000).then(async elem => {
					await elem.findElement(By.xpath('./../td[last()]/a[2]')).click();
				}, async err => {
					await driver.findElement(By.xpath('//div[@id="wrapper"]/a[@class="btn btn-mini btn-success"]')).click();
				});

// 				await driver.wait(until.elementLocated(By.xpath('//select[@name="episode[movie_id]"]')), 10000).click();
// 				await driver.findElement(By.xpath('//select[@name="episode[movie_id]"]/option[text() = "'+movie+'"]')).click();

				let episodeTitle = await driver.findElement(By.xpath('//input[@name="episode[title]"]'));
				if ((await episodeTitle.getAttribute('value')).length <= data[1].length) {
					await episodeTitle.sendKeys(data[1]);
				}

				await driver.findElement(By.xpath('//select[@name="selectSources"]')).click();
				await driver.findElement(By.xpath('//select[@name="selectSources"]/option[@value='+sources.length+']')).click();
				for (let i = 0; i < sources.length; i++) {
					await driver.wait(until.elementLocated(By.xpath('//input[@onblur="updateTextarea('+i+')"]')), 1000).sendKeys('src="'+sources[i]+'"');
				}

				await driver.findElement(By.xpath('//input[@name="commit"]')).click();
				await driver.wait(until.elementLocated(By.xpath('//div[@id="flash_notice"]')),10000).then(async elem => {
					await driver.findElement(By.xpath('//a[@class="btn btn-warning"]')).click();
				}, async err => {
					console.log(err);
				});
			}
		}
	}

	finally {
		driver.quit();
	}
})();