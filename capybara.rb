require 'capybara/dsl'
require 'selenium-webdriver'

include Capybara::DSL

require './auth.rb'

# Settings | 0: Check DB, 1: AnimeZone/KreskówkaZone, 2: Animek24, 3: CDA, 4: Google Drive, 5: FreeDisc, 8: MP4Upload, 9: fili.tv, 10: Upload to Best-Anime

if ARGV.empty?
	puts "BAUploader Script Usage: \n\n"
	puts "Type operation: "
	puts "0 - Check DB, \t\t 1 - AnimeZone/KreskówkaZone, \t 2 - Animek24, \t\t 3 - CDA, \t 4 - Google Drive"
	puts "5 - FreeDisc, \t\t 8 - MP4Upload, \t\t 9 - Fili, \t\t 10 - Upload to Best-Anime\n\n"
	puts "ruby capybara.rb 0 [Type Movie] [Start Letter]\t\t\t\t\t<- Create list not working streams on best-anime.eu"
	puts "ruby capybara.rb [Type Operation] [Series name] [Season] [Episode] [URL]\t<- Create CSV with iframes this serie"
	puts "ruby capybara.rb 10 [Filename] [How many episodes] [Start position]\t\t<- Send iframes to best-anime.eu service"
	exit
end

webs = ARGV[0].to_i
if(webs != 0 && webs != 10)
	src = ARGV[4] else src = 'https://best-anime.eu/login' end
movie = ARGV[1]
season = ARGV[2].to_i if webs!=0 && webs!=10
episode = ARGV[3].to_i if webs!=0 && webs!=10
# IF Upload to Best-Anime
if webs==10
  episodes = ARGV[2].to_i
  position = ARGV[3].to_i
elsif webs==0
  type = ARGV[1]
  letter = ARGV[2]
end

if webs!=10 && webs!=0
	file = File.new movie.to_s, "w"
elsif webs==10
	file = File.new movie.to_s, "r"
end

Capybara.register_driver :selenium do |app|
  #profile = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {
  #    "binary" => "/usr/bin/chromedriver",
  #    "args" => ['--load-extension=/home/USERNAME/.config/google-chrome/Default/Extensions/cjpalhdlnbpafiamejdnhcphjbkeiagm/1.15.18_0']
  #  }
  #)

	# With uBlock
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: Selenium::WebDriver::Chrome::Options.new(args: ['--load-extension=/home/USERNAME/.config/google-chrome/Default/Extensions/cjpalhdlnbpafiamejdnhcphjbkeiagm/1.24.4_0', '--start-maximized']))

  #Capybara::Selenium::Driver.new(app, browser: :chrome, switches: ['--load-extension=/home/USERNAME/.config/google-chrome/Default/Extensions/cjpalhdlnbpafiamejdnhcphjbkeiagm/1.23.0_0'])
end

Capybara.current_driver = :selenium
visit "https://duckduckgo.com" if webs==1
visit src

def add_record(movie, season, episode, webs, file)
	if webs==1 # AnimeZone/KreskówkaZone
		#sleep 2

		if page.title.match("anime")
			name=find(:xpath, '//div[@class="panel-body"]/h2').text[12..-2] if find(:xpath, '//div[@class="panel-body"]/h2').text[11]!=' '
			name=find(:xpath, '//div[@class="panel-body"]/h2').text[13..-2] if find(:xpath, '//div[@class="panel-body"]/h2').text[11]==' '

			if has_xpath?('//a[@class="link-play center"]')
				if season<10 && episode<10
					file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+name.to_s+';'+'src="'+find(:xpath, '//a[@class="link-play center"]')['href']+'";'+'Napisy;')
				elsif season<10
					file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+name.to_s+';'+'src="'+find(:xpath, '//a[@class="link-play center"]')['href']+'";'+'Napisy;')
				elsif episode<10
					file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+name.to_s+';'+'src="'+find(:xpath, '//a[@class="link-play center"]')['href']+'";'+'Napisy;')
				else
					file.puts(movie+';'+season.to_s+episode.to_s+' - '+name.to_s+';'+'src="'+find(:xpath, '//a[@class="link-play center"]')['href']+'";'+'Napisy;')
				end
			else
				if season<10 && episode<10
					file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+name.to_s+';'+'src="'+find(:xpath, "//div[@class='panel-body embed-container']/iframe")['src']+'";'+'Napisy;')
				elsif season<10
					file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+name.to_s+';'+'src="'+find(:xpath, "//div[@class='panel-body embed-container']/iframe")['src']+'";'+'Napisy;')
				elsif episode<10
					file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+name.to_s+';'+'src="'+find(:xpath, "//div[@class='panel-body embed-container']/iframe")['src']+'";'+'Napisy;')
				else
					file.puts(movie+';'+season.to_s+episode.to_s+' - '+name.to_s+';'+'src="'+find(:xpath, "//div[@class='panel-body embed-container']/iframe")['src']+'";'+'Napisy;')
				end
			end
		elsif page.title.match("kreskówki")
			if has_xpath?('//iframe')
				if season<10 && episode<10
					file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+find(:xpath, '//table[1]/tbody/tr[2]/td').text+';'+'src="'+find('iframe')['src']+'";'+'Lek/Dub;')
				elsif season<10
					file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+find(:xpath, '//table[1]/tbody/tr[2]/td').text+';'+'src="'+find('iframe')['src']+'";'+'Lek/Dub;')
				elsif episode<10
					file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+find(:xpath, '//table[1]/tbody/tr[2]/td').text+';'+'src="'+find('iframe')['src']+'";'+'Lek/Dub;')
				else
					file.puts(movie+';'+season.to_s+episode.to_s+' - '+find(:xpath, '//table[1]/tbody/tr[2]/td').text+';'+'src="'+find('iframe')['src']+'";'+'Lek/Dub;')
				end
			else
				if season<10 && episode<10
					file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+find(:xpath, '//table[1]/tbody/tr[2]/td').text+';'+'src="'+find(:xpath, '//*[@id="pojemnik_odcinka"]/a')['href']+'";'+'Lek/Dub;')
				elsif season<10
					file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+find(:xpath, '//table[1]/tbody/tr[2]/td').text+';'+'src="'+find(:xpath, '//*[@id="pojemnik_odcinka"]/a')['href']+'";'+'Lek/Dub;')
				elsif episode<10
					file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+find(:xpath, '//table[1]/tbody/tr[2]/td').text+';'+'src="'+find(:xpath, '//*[@id="pojemnik_odcinka"]/a')['href']+'";'+'Lek/Dub;')
				else
					file.puts(movie+';'+season.to_s+episode.to_s+' - '+find(:xpath, '//table[1]/tbody/tr[2]/td').text+';'+'src="'+find(:xpath, '//*[@id="pojemnik_odcinka"]/a')['href']+'";'+'Lek/Dub;')
				end
			end
		end
	elsif webs==2 # Animek24
		sleep 2
		if has_xpath?('//div[@id="option-1"]/iframe')
			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+find(:xpath, '//h1[@class="epih1"]').text+';'+'src="'+find(:xpath, '//div[@id="option-1"]/iframe')['src']+'";'+'Lek/Dub;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+find(:xpath, '//h1[@class="epih1"]').text+';'+'src="'+find(:xpath, '//div[@id="option-1"]/iframe')['src']+'";'+'Lek/Dub;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+find(:xpath, '//h1[@class="epih1"]').text+';'+'src="'+find(:xpath, '//div[@id="option-1"]/iframe')['src']+'";'+'Lek/Dub;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+find(:xpath, '//h1[@class="epih1"]').text+';'+'src="'+find(:xpath, '//div[@id="option-1"]/iframe')['src']+'";'+'Lek/Dub;')
			end
		end
	elsif webs==3 # CDA
		sleep 2

    sources = all(:xpath, '//div[@class="thumb-wrapper-just"]/div[@class="list-when-small tip"]/span[@class="thumbnail viewList-inline"]/span[@class="caption"]/span[@class="caption-label"]/a')

		sources.each do |source|
			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+';'+'src="https://ebd.cda.pl/1160x654/'+source['href'][25..-1]+'";'+'Lek/Dub;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+';'+'src="https://ebd.cda.pl/1160x654/'+source['href'][25..-1]+'";'+'Lek/Dub;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+';'+'src="https://ebd.cda.pl/1160x654/'+source['href'][25..-1]+'";'+'Lek/Dub;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+';'+'src="https://ebd.cda.pl/1160x654/'+source['href'][25..-1]+'";'+'Lek/Dub;')
			end

			episode+=1
		end
  elsif webs==4 # Google Drive
		sleep 2

    sources = all(:xpath, '//div[@class="iZmuQc"]/c-wiz/div') # This place maybe uncorrect prev: a-u

		sources.each do |source|
			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+';'+'src="https://drive.google.com/file/d/'+source['data-id']+'/preview";'+'Lek/Dub;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+';'+'src="https://drive.google.com/file/d/'+source['data-id']+'/preview";'+'Lek/Dub;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+';'+'src="https://drive.google.com/file/d/'+source['data-id']+'/preview";'+'Lek/Dub;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+';'+'src="https://drive.google.com/file/d/'+source['data-id']+'/preview";'+'Lek/Dub;')
			end

			episode+=1
		end
	elsif webs==5 # FreeDisc
		sleep 2

		sources = all(:xpath, '//div[@class="dir-item"]/div[@class="data"]')

		sources.each do |source|
			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+';'+'src="https://freedisc.pl/embed/video/'+source['id'][2..-1]+'";'+'Lek/Dub;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+';'+'src="https://freedisc.pl/embed/video/'+source['id'][2..-1]+'";'+'Lek/Dub;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+';'+'src="https://freedisc.pl/embed/video/'+source['id'][2..-1]+'";'+'Lek/Dub;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+';'+'src="https://freedisc.pl/embed/video/'+source['id'][2..-1]+'";'+'Lek/Dub;')
			end

			episode+=1
		end
	elsif webs==8 # MP4Upload
		sleep 2

		sources = all(:xpath, '//table[@id="files_list"]/tbody/tr[@class="filerow"]/td[2]/a')

		sources.each do |source|
			puts source
			if season<10 && episode<10
				file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+';'+'src="https://www.mp4upload.com/embed-'+source['href'][26..-1]+'.html";'+'Lek/Dub;')
			elsif season<10
				file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+';'+'src="https://www.mp4upload.com/embed-'+source['href'][26..-1]+'.html";'+'Lek/Dub;')
			elsif episode<10
				file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+';'+'src="https://www.mp4upload.com/embed-'+source['href'][26..-1]+'.html";'+'Lek/Dub;')
			else
				file.puts(movie+';'+season.to_s+episode.to_s+' - '+';'+'src="https://www.mp4upload.com/embed-'+source['href'][26..-1]+'.html";'+'Lek/Dub;')
			end

			episode+=1
		end
	elsif webs==9 # fili.tv
		sleep 2

    episode_title = find('.episode_title').text[9..-1]

    within_frame 'iframe' do
      iframe = find('iframe')
      if !iframe.text.match("Sorry") && !iframe.text.match("File was deleted") && !iframe.text.match("not found") && !iframe['src'].match("openload")
        if season<10 && episode<10
          file.puts(movie+';'+season.to_s+'x0'+episode.to_s+' - '+episode_title+';'+'src="'+iframe['src']+'";'+'Lek/Dub;')
        elsif season<10
          file.puts(movie+';'+season.to_s+'x'+episode.to_s+' - '+episode_title+';'+'src="'+iframe['src']+'";'+'Lek/Dub;')
        elsif episode<10
          file.puts(movie+';'+season.to_s+'0'+episode.to_s+' - '+episode_title+';'+'src="'+iframe['src']+'";'+'Lek/Dub;')
        else
          file.puts(movie+';'+season.to_s+episode.to_s+' - '+episode_title+';'+'src="'+iframe['src']+'";'+'Lek/Dub;')
        end
      end
    end
	end
end

case webs
  when 0  # Check DB
    #errors = ["ЗАПРАШИВАЕМОЕ","Error","Ошибка обработки видео","Niestety","link został usunięty","been","File","Video not found","Ошибка","Вы пробуете посмотреть","Wideo niedostępne","Ten plik","Film niedostępny","Not Found","We’re Sorry!","Can't find","Object"]
    errors =
    [
			["Napisy",      "v"],
			["CDA",         "usunięty"],
			["VK",					"video"],
			["Google",      "Niestety"],
			["YouTube",			"błąd"],
			["DailyMotion",	"Error"],
			["VidFile",     "Video"],
      ["MP4Upload",   "Error"],
			["VidLox",      "Error"],
			["Tune",        "video"],
			["AnyFiles",    "Wideo"],
			["MyVi",				"ЗАПРАШИВАЕМОЕ"],
			["VidTo",       "File"],
			["Reseton",			"e"],
			["SibNet",      "Видео"],
			["vShare",			"e"],
			["RapidVideo",  "Object"],
			["Vidoza",      "Support"],
			["StreaMango",  "Sorry!"],
			["StreamCherry","Sorry!"],
			["Raptu",				"e"],
			["OK",          "Video"],
			["Meta",				"e"],
			["RuTube",			"Вы пробуете посмотреть"],
			["StreamMOE",		"e"],
			["TheVideo",		"e"],
			["eStream",			"e"],
			["FreeDisc",		"Ten plik"],
			["SpeedVid",		"e"],
			["OneDrive",		"e"],
			["OpenLoad",    "Sorry"],
			["CloudVideo", 	"e"],
			["GoUnlimited",	"e"],
			["Facebook",		"Film niedostępny"],
			["Nieznany",    "v"]
    ]

		fill_in "session[email]", with: BA_LOGIN
    fill_in "session[password]", with: BA_PASSWORD
    find("input.btn").click

		while has_xpath?('//iframe[@title="recaptcha challenge"]'); sleep 5; end

    #find("h1").click;
		sleep 1; startSeries, startLetters = false, false

    all(:xpath, '//li[@class="nav-item dropdown"]').size.times do |i|                                                         # Wybieranie Typu
      startSeries=true if find(:xpath, '//li[@class="nav-item dropdown"]['+(i+1).to_s+']/a').text.match(type.to_s)
      next if startSeries==false
      #print "|"+find(:xpath, '//li[@class="nav-item dropdown"]['+(i+1).to_s+']/a').text+'|'

      find(:xpath, '//li[@class="nav-item dropdown"]['+(i+1).to_s+']/a').click; sleep 1
      all(:xpath, '//div[@class="dropdown-menu show"]/a').size.times do |j|                                                   # Przechodzenie po Literach
        if has_xpath?('//div[@class="dropdown-menu show"]/a['+(j+1).to_s+']')
          startLetters=true if find(:xpath, '//div[@class="dropdown-menu show"]/a['+(j+1).to_s+']').text.match(letter)
          next if startLetters==false
        end

        find(:xpath, '//div[@class="dropdown-menu show"]/a['+(j+1).to_s+']').click; sleep 1
        if has_xpath?('//article')
          all(:xpath, '//article').size.times do |k|                                                                          # Przechodzenie po Seriach
            if k!=0; find(:xpath, '//li[@class="nav-item dropdown"]['+(i+1).to_s+']/a').click; sleep 1;
              find(:xpath, '//div[@class="dropdown-menu show"]/a['+(j+1).to_s+']').click; sleep 1; end
            find(:xpath, '//article['+(k+1).to_s+']/div[@class="list-articles"]/a').click; sleep 1

						find(:xpath, '//article[1]/div[@class="list-articles"]/a').click; sleep 1																					# Przechodzenie po Odcinkach
            while true
							sleep 0.1; n = all(:xpath, '//div[@class="videoSwitch"]/a').size																								# Przechodzenie po Playerach
							n.times do |n|
							#all(:xpath, '//div[@class="videoSwitch"]/a').size.times do |n|                                                 # Z jakiegoś powodu n nie spada po wyjściu z pętli
                find(:xpath, '//div[@class="videoSwitch"]/a['+(n+1).to_s+']').click if n!=0
                sleep 1
                title_episode = find(:xpath, '//h3').text
                player_name = find(:xpath, '//div[@class="videoSwitch"]/a['+(n+1).to_s+']').text
                within_frame 0 do
									if has_text?("odrzucił połączenie")
										puts title_episode+": "+player_name
									else
										errors.each do |error|
											if error[0]==player_name
												puts title_episode+": "+player_name if has_text?(error[1])
												break
											end
										end
									end
                end
              end
							has_xpath?('//div[@id="RArrow"]') ? find(:xpath, '//div[@id="RArrow"]/a').click : break
						end
						puts "\n"
          end
        end
        sleep 1; find(:xpath, '//li[@class="nav-item dropdown"]['+(i+1).to_s+']/a').click
      end
    end

	when 1	# AnimeZone/KreskówkaZone
    #sleep 12 # Manual disable adblock
		while true

			sources=1
			tabPlayers =
			[
				["Myvi.ru", 			"Myvi.tv",				"ЗАПРАШИВАЕМОЕ"],
			  ["Mp4Upload.com", "Mp4upload.com",	"Error"], # The media could not be loaded
			  ["Vidlox.me", 		"Vidlox.me",			"Error"],
			  ["Sibnet", 				"Sibnet",					"видео"], # Нет такого видео
			  ["Google.com", 		"Google.com",			"Niestety"],
				["CDA.pl", 				"CDA.pl",					"link został usunięty"],
        ["ok.ru", 				"Ok.ru",					"been"],	# Video has not been found
        ["StreaMango.com","StreaMango.com",	"File"],
			  ["VidFile.net", 	"VidFile.net",		"Video not found"],
				["thevideo.me",		"thevideo.me",		"File was deleted"],
				["Meta.ua", 			"Meta.ua",				"Ошибка"],
				["Streamin.to",		"Streamin.to",		"File was deleted"],
				["Rutube",				"Rutube",					"Вы пробуете посмотреть"],
				["Vidto.me",			"Vidto.me",				"File Does not Exist"],
				["AnyFiles", 			"Anyfiles",				"Wideo niedostępne"],
				["Tune.pk",				"Tune.pk",				"video has been deactivated"],
        ["vidoza.net",		"vidoza.net",			"File"],
				["Freedisc.pl",		"Freedisc.pl",		"Ten plik"],
				["Vidzer.net",		"Vidzer.net",			"Not Found"],
        ["Facebook", 			"Facebook",				"Film niedostępny"],
				#["vShare.io", 		"vShare.io",			"Can't find the resource"],
				["videowood.tv"		"videowood.tv",		"This video doesn't exist"],
				["Cloudtime.to",	"Cloudtime.to",		"This video is not yet ready"],
				#["MovShare.net",	"MovShare.net",		"This video is not yet ready"],
				["gounlimited.to","gounlimited.to",	"Error"],
				#["openload.io",		"openload.io",		"We’re Sorry!"],
				#["flashx.tv",			"flashx.tv",			"x-x"]
			]

			if page.title.match("anime")

				tabPlayers.length.times do |index|
					break if sources==0
					all(:xpath, '//tr/td[3]/span[@class="sprites PL lang"]').size.times do |i|
						if find(:xpath, '//tbody/tr['+(i+1).to_s+']/td[1]').text.match(tabPlayers[index][0])
							execute_script 'window.scrollBy(0,200)'
							find(:xpath, "//tbody/tr["+(i+1).to_s+"]/td[4]/button[@class='btn btn-xs btn-success play']").click
							sleep 4
              page.driver.browser.switch_to().window(page.driver.browser.window_handles.last)

							if has_xpath?("//div[@class='panel-body embed-container']/iframe")
								next_player=false;
								within_frame 0 do
									next_player = has_text?(tabPlayers[index][2])
								end; next if next_player
							end

              #next if find(:xpath, "//div[@class='panel-body embed-container']/iframe").text.match(tabPlayers[index][2])
							sources-=1
							add_record movie, season, episode, webs, file
							break
						end
					end
				end

				episode+=1
				if has_xpath?('//li[@class="next"]/a')
					find(:xpath, '//li[@class="next"]/a').click
				else
          # Close App
					break
				end

			elsif page.title.match("kreskówki")

				tabPlayers.length.times do |index|
					break if sources==0
					all(:xpath, '//tbody/tr/td[4]/span[@class="sprites pl center"]').size.times do |i|
						# Remove fullscreen invisible ad
						#find_link('', options = {href: /1680&amp;scrHeight=1050/}).click; sleep 2
						#page.execute_script("")

						if find(:xpath, '//tbody/tr[@class="wiersz"]['+(i+1).to_s+']/td[2]').text.match(tabPlayers[index][1]) && !find(:xpath, '//tbody/tr[@class="wiersz"]['+(i+1).to_s+']/td[3]').text.match("Napisy")
							find(:xpath, '//tbody/tr[@class="wiersz"]['+(i+1).to_s+']/td[5]/a').click

              while !(has_xpath?('//iframe') || has_xpath?('//*[@id="pojemnik_odcinka"]/a')); find(:xpath, '//tbody/tr[@class="wiersz"]['+(i+1).to_s+']/td[5]/a').click; sleep 2; end
							
							if has_xpath?('//iframe')
								next if find('iframe').text.match(tabPlayers[index][2])
								puts find(:xpath, '//table[1]/tbody/tr[2]/td').text+"\t"+find('iframe')['src']
							else
								puts find(:xpath, '//table[1]/tbody/tr[2]/td').text+"\t"+find(:xpath, '//*[@id="pojemnik_odcinka"]/a')['href']
							end
								
							sources-=1
							add_record movie, season, episode, webs, file
							break
						end
					end
				end

				episode+=1
				if has_xpath?('//table[4]/tbody/tr/td[3]/a')
          url=current_url
					while url==current_url; find(:xpath, '//table[4]/tbody/tr/td[3]/a').click; end
				else
					# Close App
					break
				end

			end
		end
	when 2	# Animek24
		sleep 1
		while true
			#sleep 11
			add_record movie, season, episode, webs, file
			episode+=1
			break if has_xpath?('//div[@class="pag_episodes"]/div[@class="item"][3]/a[@class="nonex"]')
			find(:xpath, '//div[@class="pag_episodes"]/div[@class="item"][3]/a').click
		end
	when 3	# CDA
		sleep 1
    if has_xpath?('//h3[@class="text-center register-name"]')
      find(:xpath, '//li[@class="dropdown"]/a[@class="dropdown-toggle bold-menu"]').click
      fill_in "username", with: CDA_LOGIN
      fill_in "password", with: CDA_PASSWORD
      find(:xpath, '//div[@class="col-md-5"]/input').click
    end
    #while has_xpath?('//iframe'); sleep 10; end

    find(:xpath, '//div[@class="btn-group pull-right"]/div[2]').click
		find(:xpath, '//li[@class="ListaMalychFolderow"]/a').click

		add_record movie, season, episode, webs, file
		while has_xpath?('//div[@class="paginationControl"]/a')
			find(:xpath, '//div[@class="paginationControl"]/a').click
			episode=episode+36
			add_record movie, season, episode, webs, file
		end
  when 4	# Google Drive
		sleep 1
		add_record movie, season, episode, webs, file
	when 5	# FreeDisc
		sleep 1
		add_record movie, season, episode, webs, file
	when 8	#MP4Upload
		sleep 1
		fill_in "login", with: MP4U_LOGIN
		fill_in "password", with: MP4U_PASSWORD
		find(:xpath, '//input[@type="submit"]').click

		visit src

		find(:xpath, '//table[@id="files_list"]/tbody/tr[1]/th[2]/a').click
		find(:xpath, '//table[@id="files_list"]/tbody/tr[1]/th[2]/a').click
		add_record movie, season, episode, webs, file
	when 9	# Fili.tv
		sleep 1

		while true
			execute_script('return document.getElementsByClassName("overlay")[0].style.maxHeight = "none";')
			while has_xpath?("//div[@class='overlay']/ul/a["+episode.to_s+"]")
				find(:xpath, "//div[@class='overlay']/ul/a["+episode.to_s+"]").click

				if has_xpath?("//ul[2]/li[@class='line']/div[2]/div/span")
					(all(:xpath, "//ul[2]/li[@class='line']/div[2]/div/span").size).times do |num_src|
            next if num_src==0
						next if find(:xpath, "//li[@class='line']["+num_src.to_s+"]/div[1]/span[1]").text == "openload"
            find(:xpath, "//li[@class='line']["+num_src.to_s+"]/div[2]/div/span").click
						while has_text?('Udowodnij, że jesteś człowiekiem.'); sleep 2; end
            within_frame 'iframe' do
              while has_no_xpath?('//iframe'); sleep 2; end
              while has_xpath?('//iframe[@title="Zadanie reCAPTCHA"]'); sleep 5; end
            end
						add_record movie, season, episode, webs, file
					end
				end

				episode+=1
			end
			season+=1
			if has_xpath?("//div[@title='Przechodzi do następnego sezonu']")
				episode = 1
				find(:xpath, "//div[@title='Przechodzi do następnego sezonu']").click
			else
				# Close App
				break
			end
		end
	when 10	# Upload to Best-Anime
    fill_in "session[email]", with: BA_LOGIN
    fill_in "session[password]", with: BA_PASSWORD
    find("input.btn").click

		while has_xpath?('//iframe[@title="recaptcha challenge"]'); sleep 5; end

		find(:xpath, '//*[@id="navbarCollapse"]/ul[2]/a').click
		sleep 3
		find(:xpath, '//*[@id="navbarCollapse"]/ul[2]/div/a[1]').click
		sleep 3
		find(:xpath, "//form/input[2]").set(ARGV[1])
		find(:xpath, "//form/input[3]").click
		all(:xpath, "//tr/td[8]").size.times do |i|
			if find(:xpath, "//tr["+(i+1).to_s+"]/td[4]").text==movie
				find(:xpath, "//tr["+(i+1).to_s+"]/td[8]/a[1]").click; break
			end
		end
		sleep 1

		#	Start Upload
		movie, title, src, version = [], [], [], []

		episodes.times do |loop|
			num_src = 1

			file.each do |row|
				if loop>0 && num_src == 1
					movie_copy=movie.last; title_copy=title.last; src_copy=src.last; version_copy=version.last
					movie.clear; title.clear; src.clear; version.clear
					movie[0]=movie_copy; title[0]=title_copy; src[0]=src_copy; version[0]=version_copy
					num_src+=1
				end
																						movie[num_src-1] = row[0,row.index(";")]
					row = row[row.index(";")+1..-1];	title[num_src-1] = row[0,row.index(";")]
					row = row[row.index(";")+1..-1];	src[num_src-1] = row[0,row.index(";")]
					row = row[row.index(";")+1..-1];	version[num_src-1] = row[0,row.index(";")]

				if num_src==1
					num_src+=1
					next
				end

				if title[num_src-1]!=title[num_src-2]
					break;
				end
				num_src+=1
			end

			# Control end file for one source
			if num_src==1
				num_src+=1; movie[0], title[0], src[0], version[0] = movie[1], title[1], src[1], version[1]
			end

			# Choose update episode or create new
			if has_text?(title[0][0..6])
				all(:xpath, '//tr').size.times do |i|
					if find(:xpath, "//tr["+(i+1).to_s+"]//td[3]").text.match(title[0][0..3])
						find(:xpath, '//tr['+(i+1).to_s+']/td[6]/a[2]').click
						break
					end
				end
			else
				find(:xpath, "//nav/div/ul[2]/li[1]/a").click
			end

			sleep 1
			select(ARGV[1], from: "episode_movie_id")
      fill_in "episode_title", with: title[0].to_s
      select(position.to_s, from: "episode_position")
			if (num_src-1)>=6
        select("Sześć", from: "selectSources")
			elsif (num_src-1)==5
				select("Pięć", from: "selectSources")
			elsif (num_src-1)==4
				select("Cztery", from: "selectSources")
			elsif (num_src-1)==3
				select("Trzy", from: "selectSources")
			elsif (num_src-1)==2
				select("Dwa", from: "selectSources")
			elsif (num_src-1)==1
				select("Jedno", from: "selectSources")
			end

			sleep 1
			#execute_script("window.scrollBy(0,400)")

			(num_src-1).times do |i|
				find(:xpath, "//tr[@class='generated']["+(i+1).to_s+"]/td[2]/div/input[@class='form-control iframe-gen']").set(src[i].to_s)
				find(:xpath, '//tr[@class="generated"]['+(i+1).to_s+']/td[2]/div').select("Napisy") if version[i] == "Napisy"

				sleep 1
				if i==5
					break
				end
			end

			find(:xpath, '//input[@name="commit"]').click
			sleep 1
			find(:xpath, '//a[@class="btn btn-warning"]').click

			position+=1
		end
	else
		puts "webs argument not walid"
end

Capybara.current_session.driver.quit
