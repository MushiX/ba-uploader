#!/bin/sh

#echo "Podaj rozszerzenie źródłowe: "
#read ext_src
echo "Podaj ścieżkę do multimedi (/.../): "
read track
echo "Podaj rozszerzenie docelowe (.xxx): "
read ext_des
#if [ $ext_des = .mp3 ]
#then
	echo "Podaj dodatkowe parametry wyjściowe (pusto-domyślne): "
	read parameters
	if [ "$parameters" != '' ]
	then
		parameters="$parameters "
	fi
#fi

for plik in "$track"*
do
if [ ${plik:(-4):4} = .mp4 ] || [ ${plik:(-4):4} = .avi ] || [ ${plik:(-4):4} = .mkv ] || [ ${plik:(-4):4} = .flv ] || [ ${plik:(-4):4} = .mp3 ] || [ ${plik:(-4):4} = .wav ] || [ ${plik:(-4):4} = .ogg ] && [ ${plik:(-4):4} != $ext_des ]
then
	echo "ffmpeg -i \"$plik\" $parameters\"${plik:0:(-4)}$ext_des\""
else
	if [ ${plik:(-5):5} = .flac ] || [ ${plik:(-5):5} = .webm ] && [ ${plik:(-5):5} != $ext_des ]
	then
		echo "ffmpeg -i \"$plik\" $parameters\"${plik:0:(-5)}$ext_des\""
	fi
fi
done

echo "Kontynuować? [t/n]: "
read choose

for plik in "$track"*
do
if [ ${plik:(-4):4} = .mp4 ] || [ ${plik:(-4):4} = .avi ] || [ ${plik:(-4):4} = .mkv ] || [ ${plik:(-4):4} = .flv ] || [ ${plik:(-4):4} = .mp3 ] || [ ${plik:(-4):4} = .wav ] || [ ${plik:(-4):4} = .ogg ] && [ ${plik:(-4):4} != $ext_des ] && [ $choose = t ]
then
	ffmpeg -i "$plik" $parameters"${plik:0:(-4)}$ext_des"
else
	if [ ${plik:(-5):5} = .flac ] || [ ${plik:(-5):5} = .webm ] && [ ${plik:(-5):5} != $ext_des ] && [ $choose = t ]
	then
		ffmpeg -i "$plik" $parameters"${plik:0:(-5)}$ext_des"
	fi
fi
done