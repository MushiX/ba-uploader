require 'capybara/dsl'
require 'selenium-webdriver'

include Capybara::DSL

require './auth.rb'

if ARGV.empty?
	puts "BAUploader Download script usage: \n\n"
	puts "ruby downloader.rb [File name]"
end

movie = ARGV[0]

Capybara.register_driver :selenium do |app|
	Capybara::Selenium::Driver.new(app, browser: :chrome, options: Selenium::WebDriver::Chrome::Options.new(args: ['--load-extension=/home/mushix/.config/google-chrome/Default/Extensions/cjpalhdlnbpafiamejdnhcphjbkeiagm/1.27.10_0', '--start-maximized']))
end

Capybara.current_driver = :selenium

def getListSources(fileName)
	file = File.new fileName.to_s, "r"

	data = []

	file.each do |row|
		data << {
			'title' => row[row.index(';')+1, row.index(';src="')-(row.index(';')+1)],
			'source' => row[row.index('src="')+5, row.index('";')-(row.index('src="')+5)]
		}
	end

	return data
end

data = getListSources(movie);

data.each do |data|
	case data['source']
		when /.drive.google.com./
			id = data['source'][data['source'].index('/d/')+3, data['source'].index('/preview')-(data['source'].index('/d/')+3)]
			url = "https://docs.google.com/uc?export=download&id="+id.to_s
			link = 'https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "'+url.to_s+"\" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\\1\\n/p')&id="+id.to_s
			command = 'wget --load-cookies /tmp/cookies.txt "'+link+'" -O "'+movie.to_s+' - '+data['title']+'" && rm -rf /tmp/cookies.txt'
			system(command)

		when /.cda.pl./
			visit data['source']
			sleep 1

			if has_xpath?('//div[@class="wrapqualitybtn"]')
				if has_xpath?('//div[@class="wrapqualitybtn"]/a[@data-quality="hd"]')
					find(:xpath, '//div[@class="wrapqualitybtn"]/a[@data-quality="hd"]').click
				elsif has_xpath?('//div[@class="wrapqualitybtn"]/a[@data-quality="sd"]')
					find(:xpath, '//div[@class="wrapqualitybtn"]/a[@data-quality="sd"]').click
				elsif has_xpath?('//div[@class="wrapqualitybtn"]/a[@data-quality="lq"]')
					find(:xpath, '//div[@class="wrapqualitybtn"]/a[@data-quality="lq"]').click
				elsif has_xpath?('//div[@class="wrapqualitybtn"]/a[@data-quality="vl"]')
					find(:xpath, '//div[@class="wrapqualitybtn"]/a[@data-quality="vl"]').click
				end
			end
			sleep 1

			userAgent = '--user-agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"'
			url = find(:xpath, '//span[@class="pb-video-player-content"]/video')['src']
			system('wget', userAgent, url, '-O', movie.to_s+' - '+data['title'])

		when /.mp4upload.com./
			puts "mp4upload"

		when /.freedisc.pl./
			visit data['source']
			sleep 1

			while has_xpath?('//*[@id="profileFiles"]/div[2]')
				sleep 2
			end

			find(:xpath, '//*[@id="video_player"]/div[10]/div[1]/div/div/div[2]/div').click; sleep 1
                  
			url = find('video')['src']
		  userAgent = '--user-agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"'
		  referer = '--referer="http://reseton.pl/static/player/v612/jwplayer.flash.swf"'
			command = "wget #{userAgent} #{referer} -c #{url} -O \"#{movie} - #{data['title']}\""

			system(command)

		else
			puts "this hosting is not supported"
	end
end
